(function ($, drupalSettings) {
  Drupal.behaviors.FlipClock = {
    attach: function (context, settings) {
      // Init all clocks.
      for (id in settings.flipClock.instances) {
        _flipclock_init(id, settings.flipClock.instances[id], context);
      }
    }
  };

  function _flipclock_init(id, optionset, context) {
    let timestamp = new Date(optionset.timestamp * 1000).getTime();
    let currentTimestamp = new Date().getTime();
    let seconds = 0;
    if (timestamp > currentTimestamp) {
      seconds = (timestamp - currentTimestamp) / 1000;
    } else {
      seconds = (currentTimestamp - timestamp) / 1000;
    }

    if (optionset.options.clockFace === 'TwentyFourHourClock' || optionset.options.clockFace === 'TwelveHourClock') {
      seconds = null;
    }

    // Init timer.
    $('#' + id).FlipClock(seconds, optionset.options);
  }
})(jQuery, drupalSettings);